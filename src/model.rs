use std::{
    fs::File,
    io::{BufRead, BufReader},
    vec::Vec,
};

#[derive(Debug)]
pub struct Model {
    checks: Vec<Vec<i32>>,
}

impl Model {
    pub fn new() -> Self {
        Model { checks: Vec::new() }
    }

    pub fn load(&mut self, filename: &str) {
        self.checks.clear();

        let f = File::open(filename).unwrap();
        let reader = BufReader::new(f);

        for line_res in reader.lines() {
            let line = line_res.unwrap();

            let checks_line = line
                .trim()
                .split_whitespace()
                .flat_map(|f| str::parse::<i32>(f))
                .collect::<Vec<_>>();
            self.checks.push(checks_line);
        }
    }

    pub fn get(&self, sys_id: i32, check_id: i32) -> &i32 {
        &self.checks[(sys_id - 1) as usize][(check_id - 1) as usize]
    }

    pub fn checks_count(&self) -> i32 {
        if self.checks.is_empty() {
            0
        } else {
            self.checks.first().unwrap().len() as i32
        }
    }

    pub fn sys_count(&self) -> i32 {
        self.checks.len() as i32
    }
}
