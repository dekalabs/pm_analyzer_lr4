use crate::model::Model;

pub mod graphs;
pub mod model;

fn main() {
    let mut model = Model::new();
    model.load("model.tsv");

    graphs::build_add_graphs(&model);
}
