use std::{
    fs::{create_dir_all, File},
    io::{BufWriter, Write},
    mem::swap,
};

use crate::model::Model;

pub fn build_add_graphs(model: &Model) {
    let check_size = model.checks_count();
    let mut check_order = (1..=check_size).collect::<Vec<i32>>();

    loop {
        build_graph(model, &check_order);

        let mut j = check_size - 2;
        while j != -1 && check_order[j as usize] >= check_order[(j + 1) as usize] {
            j = j - 1;
        }
        if j == -1 {
            break;
        }

        let mut k = check_size - 1;
        while check_order[j as usize] >= check_order[k as usize] {
            k = k - 1;
        }

        check_order.swap(j as usize, k as usize);
        let mut l = j + 1;
        let mut r = check_size - 1;
        while l < r {
            check_order.swap(l as usize, r as usize);
            l += 1;
            r -= 1;
        }
    }
}

pub fn build_graph(model: &Model, check_seq: &Vec<i32>) {
    create_dir_all("./graphs/").unwrap();
    let name = graph_name(&check_seq);
    let path = format!("./graphs/{}.dot", name);
    let file = File::create(path).unwrap();
    let mut writer = BufWriter::new(file);

    let sys_c = model.sys_count();
    let mut state_zero = Vec::new();
    for s in 1..=sys_c {
        state_zero.push(s);
    }

    let graph_desc = graph_build_step(&state_zero, model, &check_seq);

    write!(&mut writer, "digraph G {{\n{}\n}}", &graph_desc).unwrap();
}

fn graph_build_step(state: &Vec<i32>, model: &Model, check_seq: &Vec<i32>) -> String {
    let mut out_string = String::new();
    let (t, d) = node_desc(state);
    out_string.push_str(&d);
    out_string.push_str(";\n");

    if check_seq.is_empty() || state.len() == 1 {
        return out_string;
    }

    for c in check_seq {
        let (s, f, cmplt) = transverse(state, model, c.clone());
        if cmplt {
            let mut new_check_seq = check_seq.clone();
            let idx_to_remove = new_check_seq.iter().position(|x| *x == *c).unwrap();
            new_check_seq.remove(idx_to_remove);

            let (title_left, _) = node_desc(&s);
            out_string.push_str(&format!(
                "{} -> {} [label=\"PI_{}^1\"]\n",
                &t, &title_left, c
            ));

            let (title_right, _) = node_desc(&f);
            out_string.push_str(&format!(
                "{} -> {} [label=\"PI_{}^2\"]\n",
                &t, &title_right, c
            ));

            let left_str = graph_build_step(&s, model, &new_check_seq);
            let right_str = graph_build_step(&f, model, &new_check_seq);

            out_string.push_str(&format!("{}\n", left_str));
            out_string.push_str(&format!("{}\n", right_str));

            break;
        }
    }

    out_string
}

fn graph_name(check_seq: &Vec<i32>) -> String {
    let mut out = String::new();
    for v in check_seq {
        out.push_str(&v.to_string());
    }
    out
}

fn node_desc(state: &Vec<i32>) -> (String, String) {
    let mut out_string_title = String::from("z");
    for v in state {
        out_string_title.push_str(&v.to_string());
        out_string_title.push('z');
    }

    let mut out_string_def = String::new();
    out_string_def.push_str(&out_string_title);
    out_string_def.push_str("[label=\"{");

    let vec_text = state
        .iter()
        .map(|x| format!("E_{}", x))
        .collect::<Vec<_>>()
        .join(",");
    out_string_def.push_str(&vec_text);
    out_string_def.push_str("}\"]");
    (out_string_title, out_string_def)
}

fn transverse(state: &Vec<i32>, model: &Model, check: i32) -> (Vec<i32>, Vec<i32>, bool) {
    let (mut succ, mut fail) = (Vec::new(), Vec::new());

    for sys_id in state {
        let val = model.get(sys_id.clone(), check);
        match val {
            -1 => fail.push(sys_id.clone()),
            1 => succ.push(sys_id.clone()),
            _ => {
                panic!("Unexpected value")
            }
        }
    }

    let mut transverse_complete = true;
    if succ.len() == state.len() || fail.len() == state.len() {
        transverse_complete = false;
    }

    (succ, fail, transverse_complete)
}
